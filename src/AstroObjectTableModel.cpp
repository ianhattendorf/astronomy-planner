#include "AstroObjectTableModel.hpp"
#include "AstroHelper.hpp"
#include "Database.hpp"
#include "TimeKeeper.hpp"
#include <QDateTime>
#include <QDebug>
#include <QMessageBox>
#include <QSqlQuery>
#include <algorithm>
#include <cmath>
#include <gsl/gsl_util>

bool AstroObject::operator==(const AstroObject &other) const {
  return id == other.id && object_id == other.object_id &&
         preferred_survey_image == other.preferred_survey_image &&
         common_names == other.common_names && ra == other.ra && dec == other.dec &&
         maj_ax == other.maj_ax && min_ax == other.min_ax && b_mag == other.b_mag &&
         v_mag == other.v_mag;
}

AstroObjectTableModel::AstroObjectTableModel(QObject *parent,
                                             AstroObjectRepository object_repository,
                                             TimeKeeper *time_keeper)
    : QAbstractTableModel(parent), object_repository_(std::move(object_repository)),
      time_keeper_(time_keeper == nullptr ? new TimeKeeper{this} : time_keeper) {
  set_headers();

  refresh_objects();

  connect(time_keeper_, &TimeKeeper::offset_changed, this, &AstroObjectTableModel::tick);
  connect(time_keeper_, &TimeKeeper::utc_offset_changed, this, &AstroObjectTableModel::tick);

  update_observables();
}

int AstroObjectTableModel::rowCount(const QModelIndex & /*parent*/) const {
  return astro_objects_.size();
}

int AstroObjectTableModel::columnCount(const QModelIndex & /*parent*/) const {
  return headers_.size();
}

QVariant AstroObjectTableModel::data(const QModelIndex &index, int role) const {
  if (!index.isValid() || index.row() >= astro_objects_.size() ||
      index.column() >= columnCount(QModelIndex())) {
    return {};
  }

  const AstroObject &astro_object = astro_objects_.at(index.row()).object;
  if (role == Qt::DisplayRole || role == Qt::EditRole) {
    const HeaderType header_type = headers_[index.column()].type;
    switch (header_type) {
    case HeaderType::Id:
      return astro_object.id;
    case HeaderType::ObjectId:
      return astro_object.object_id;
    case HeaderType::Names:
      return astro_object.common_names;
    case HeaderType::Ra:
      return astro_object.ra == 0.0 ? QVariant{} : astro_object.ra;
    case HeaderType::Dec:
      return astro_object.dec == 0.0 ? QVariant{} : astro_object.dec;
    case HeaderType::MajorAxis:
      return astro_object.maj_ax == 0.0 ? QVariant{} : astro_object.maj_ax;
    case HeaderType::MinorAxis:
      return astro_object.min_ax == 0.0 ? QVariant{} : astro_object.min_ax;
    case HeaderType::BMag:
      return astro_object.b_mag == 0.0 ? QVariant{} : astro_object.b_mag;
    case HeaderType::VMag:
      return astro_object.v_mag == 0.0 ? QVariant{} : astro_object.v_mag;
      // TODO cache Az/Alt?
    case HeaderType::Az:
      [[fallthrough]];
    case HeaderType::Alt:
      [[fallthrough]];
    case HeaderType::HourAngle:
      [[fallthrough]];
    case HeaderType::Rise:
      [[fallthrough]];
    case HeaderType::Transit:
      [[fallthrough]];
    case HeaderType::Set:
      [[fallthrough]];
    case HeaderType::Visible: {
      const AstroObserved observed =
          AstroHelper::icrs_to_observed(astro_object.ra, astro_object.dec, astrom_);
      if (header_type == HeaderType::Az) {
        return observed.azimuth;
      }
      if (header_type == HeaderType::Alt) {
        return observed.altitude(true);
      }
      if (header_type == HeaderType::Visible) {
        return observed.altitude(true) > 0
                   ? "Yes"
                   : "No"; // TODO configurable visibility ? (e.g. > 30 degrees = visible)
      }
      if (header_type == HeaderType::HourAngle) {
        return observed.hour_angle;
      }
      // TODO cache/deduplicate rise/transit/set
      // TODO use delegate for formatting
      // TODO correct rise/transit/set sidereal to local time
      if (header_type == HeaderType::Rise) {
        const auto rise_transit_set = AstroHelper::rise_transit_set_lst(
            astro_object.ra, astro_object.dec, observing_parameters_.latitude,
            AstroHelper::dd_to_rad(0.0));
        const std::variant<double, RiseSet> rts_rad = std::get<0>(rise_transit_set);

        return std::visit(
            [this](auto &&arg) -> QString {
              using T = std::decay_t<decltype(arg)>;
              if constexpr (std::is_same_v<T, RiseSet>) {
                if (arg == RiseSet::Always) {
                  return "Always";
                }
                if (arg == RiseSet::Never) {
                  return "Never";
                }
              } else if constexpr (std::is_same_v<T, double>) {
                const double local_offset = arg - local_sidereal_time_;

                return time_keeper_->local_offset_time()
                    .addSecs(AstroHelper::rad_to_hms(local_offset).seconds_since_start().count())
                    .toString("hh:mm:ss");
              }
              throw std::runtime_error("Unknown RiseSet");
            },
            rts_rad);
      }
      if (header_type == HeaderType::Transit) {
        const auto rise_transit_set = AstroHelper::rise_transit_set_lst(
            astro_object.ra, astro_object.dec, observing_parameters_.latitude,
            AstroHelper::dd_to_rad(0.0));
        const double rts_rad = std::get<1>(rise_transit_set);

        const double local_offset = rts_rad - local_sidereal_time_;

        return time_keeper_->local_offset_time()
            .addSecs(AstroHelper::rad_to_hms(local_offset).seconds_since_start().count())
            .toString("hh:mm:ss");
      }
      if (header_type == HeaderType::Set) {
        const auto rise_transit_set = AstroHelper::rise_transit_set_lst(
            astro_object.ra, astro_object.dec, observing_parameters_.latitude,
            AstroHelper::dd_to_rad(0.0));
        const std::variant<double, RiseSet> rts_rad = std::get<2>(rise_transit_set);

        return std::visit(
            [this](auto &&arg) -> QString {
              using T = std::decay_t<decltype(arg)>;
              if constexpr (std::is_same_v<T, RiseSet>) {
                if (arg == RiseSet::Always) {
                  return "Always";
                }
                if (arg == RiseSet::Never) {
                  return "Never";
                }
              } else if constexpr (std::is_same_v<T, double>) {
                const double local_offset = arg - local_sidereal_time_;

                return time_keeper_->local_offset_time()
                    .addSecs(AstroHelper::rad_to_hms(local_offset).seconds_since_start().count())
                    .toString("hh:mm:ss");
              }
              throw std::runtime_error("Unknown RiseSet");
            },
            rts_rad);
      }
      return {};
    }
    case HeaderType::TransitAlt:
      return AstroHelper::transit_alt(astro_object.dec, observing_parameters_.latitude);
    default:
      throw std::runtime_error("Unhandled header type");
    }
  } else if (role == Qt::UserRole) {
    return astro_object.preferred_survey_image.has_value()
               ? QVariant::fromValue(astro_object.preferred_survey_image.value())
               : QVariant{};
  }
  return {};
}

QVariant AstroObjectTableModel::headerData(int section, Qt::Orientation orientation,
                                           int role) const {
  if (orientation == Qt::Horizontal) {
    Expects(section >= 0 && section < columnCount(QModelIndex()));
  } else if (orientation == Qt::Vertical) {
    Expects(section >= 0 && section < rowCount(QModelIndex()));
  }

  if (role == Qt::DisplayRole || role == Qt::EditRole) {
    if (orientation == Qt::Horizontal) {
      return headers_[section].header;
    }
    if (orientation == Qt::Vertical) {
      switch (astro_objects_.at(section).status) {
      case SqlStatus::Insert:
        return "*";
      case SqlStatus::Delete:
        return "!";
      case SqlStatus::None:
        return QString::number(section + 1);
      }
    }
  } else if (role == Qt::ToolTipRole) {
    if (orientation == Qt::Horizontal) {
      return headers_[section].tooltip;
    }
  }

  return {};
}

bool AstroObjectTableModel::setData(const QModelIndex &index, const QVariant &value, int role) {
  if (!index.isValid() || index.row() >= astro_objects_.size() ||
      index.column() >= columnCount(QModelIndex())) {
    return true; // false?
  }

  SqlCache<AstroObject> &astro_object_cache = astro_objects_[index.row()];
  if (role == Qt::EditRole) {
    bool set_success = false;
    const HeaderType header_type = headers_[index.column()].type;
    switch (header_type) {
    case HeaderType::ObjectId: {
      const QString new_object_id = value.toString().toUpper();
      const bool object_exists =
          new_object_id.isEmpty() ? false
                                  : std::find_if(astro_objects_.cbegin(), astro_objects_.cend(),
                                                 [&new_object_id](const auto &object) {
                                                   return object.object.object_id == new_object_id;
                                                 }) != astro_objects_.cend();
      if (object_exists) {
        QMessageBox box;
        box.setText(tr("Object with that Object ID already exists."));
        box.setIcon(QMessageBox::Warning);
        box.exec();
      } else {
        astro_object_cache.object.object_id = new_object_id;
        set_success = true;
      }
      break;
    }
    default:
      // ignore
      break;
    }

    if (set_success) {
      astro_object_cache.dirty = true;
      fill_astro_object_data(astro_object_cache.object);
      emit dataChanged(index, createIndex(index.row(), columnCount(QModelIndex()) - 1));
    }

    return set_success;
  }
  if (role == Qt::UserRole) {
    astro_object_cache.dirty = true;
    Expects(value.userType() == qMetaTypeId<SkySurvey>());
    astro_object_cache.object.preferred_survey_image = value.value<SkySurvey>();
    emit dataChanged(index, createIndex(index.row(), columnCount(QModelIndex()) - 1));
    return true;
  }

  return true;
}

bool AstroObjectTableModel::insertRows(int row, int count, const QModelIndex & /*index*/) {
  beginInsertRows(QModelIndex(), row, row + count - 1);

  for (int i = 0; i < count; ++i) {
    astro_objects_.insert(row, SqlCache<AstroObject>{{}, SqlStatus::Insert});
  }

  endInsertRows();
  return true;
}

bool AstroObjectTableModel::removeRows(int row, int count, const QModelIndex & /*index*/) {
  Expects(count == 1);
  if (row < 0 || row >= astro_objects_.size()) {
    qWarning().nospace() << "Tried to remove invalid row: " << row;
    return false;
  }
  if (astro_objects_.at(row).object.id == -1) {
    beginRemoveRows({}, row, row);
    astro_objects_.removeAt(row);
    endRemoveRows();
  } else {
    astro_objects_[row].status = SqlStatus::Delete;
    emit headerDataChanged(Qt::Vertical, row, row);
  }

  return true;
}

Qt::ItemFlags AstroObjectTableModel::flags(const QModelIndex &index) const {
  if (!index.isValid() || index.row() >= astro_objects_.size() ||
      index.column() >= columnCount(QModelIndex())) {
    return QAbstractTableModel::flags(index);
  }

  Qt::ItemFlags flags = Qt::NoItemFlags;
  flags |= Qt::ItemIsSelectable;
  flags |= Qt::ItemIsEnabled;
  if (index.column() == header_column(HeaderType::ObjectId)) {
    flags |= Qt::ItemIsEditable;
  }

  return flags;
}

void AstroObjectTableModel::set_observing_parameters(ObservingParameters observing_parameters) {
  observing_parameters_ = std::move(observing_parameters);
  qDebug() << "Observing params updated";
}

bool AstroObjectTableModel::is_dirty() const {
  return std::find_if(astro_objects_.cbegin(), astro_objects_.cend(), [](const auto &object) {
           return object.dirty || object.status != SqlStatus::None;
         }) != astro_objects_.cend();
}

void AstroObjectTableModel::revertAll() {
  beginResetModel();
  refresh_objects();
  endResetModel();
}

bool AstroObjectTableModel::submitAll() {
  bool success = true;

  object_repository_.begin_transaction();

  for (int i = astro_objects_.size(); i-- > 0;) {
    auto &astro_object = astro_objects_[i];
    switch (astro_object.status) {
    case SqlStatus::Delete: {
      if (object_repository_.del(astro_object.object)) {
        beginRemoveRows({}, i, i);
        astro_objects_.removeAt(i);
        endRemoveRows();
      } else {
        qWarning() << "Query exec failed";
        success = false;
      }
      break;
    }
    case SqlStatus::Insert: {
      if (object_repository_.create(astro_object.object)) {
        astro_object.status = SqlStatus::None;
        astro_object.dirty = false;
        emit dataChanged(createIndex(i, header_column(HeaderType::Id)),
                         createIndex(i, header_column(HeaderType::Id)));
        emit headerDataChanged(Qt::Vertical, i, i);
      } else {
        qWarning() << "Query exec failed";
        success = false;
      }
      break;
    }
    case SqlStatus::None: {
      if (astro_object.dirty) {
        if (object_repository_.update(astro_object.object)) {
          astro_object.dirty = false;
        } else {
          qWarning() << "Query exec failed";
          success = false;
        }
      }
      break;
    }
    }
  }

  if (success) {
    object_repository_.end_transaction();
  }

  return success;
}

void AstroObjectTableModel::tick() {
  update_observables();
}

void AstroObjectTableModel::refresh_objects() {
  astro_objects_.clear();

  std::vector<AstroObject> objects = object_repository_.fetch_all();

  for (auto &object : objects) {
    astro_objects_.append({std::move(object)});
  }
}

void AstroObjectTableModel::fill_astro_object_data(AstroObject &astro_object) const {
  object_repository_.fill_astro_object_data(astro_object);
}

void AstroObjectTableModel::update_observables() {
  // TODO Do we need to lock?
  astrom_ = AstroHelper::get_astrom(observing_parameters_, time_keeper_->offset());
  greenwich_sidereal_time_ = AstroHelper::get_gast(time_keeper_->offset());
  local_sidereal_time_ = greenwich_sidereal_time_ + observing_parameters_.longitude;
  // TODO order of columns might change
  emit dataChanged(createIndex(0, header_column(HeaderType::Az)),
                   createIndex(rowCount(QModelIndex()) - 1, header_column(HeaderType::Set)));
}

void AstroObjectTableModel::set_headers() {
  headers_ = {
      {HeaderType::Id, tr("ID"), tr("SQLite table identifier")},
      {HeaderType::ObjectId, tr("Object ID"),
       tr("Object identifier (M, NGC, or IC), e.g. M31 or NGC7000")},
      {HeaderType::Names, tr("Common Names"), tr("Common names of the object")},
      {HeaderType::Ra, tr("RA"), tr("Right Ascension")},
      {HeaderType::Dec, tr("Dec"), tr("Declination")},
      {HeaderType::Az, tr("Az"), tr("Observed Azimuth")},
      {HeaderType::Alt, tr("Alt"), tr("Observed Altitude")},
      {HeaderType::Visible, tr("Visible"), tr("Object is visible")},
      {HeaderType::HourAngle, tr("HA"), tr("Hour Angle of the object")},
      {HeaderType::Rise, tr("Rise"), tr("Time the object rises")},
      {HeaderType::Transit, tr("Transit"),
       tr("Time the object transits (reaches the highest point in the sky)")},
      {HeaderType::TransitAlt, tr("Transit Alt"),
       tr("Altitude at which the object transits (reaches the highest point in the sky)")},
      {HeaderType::Set, tr("Set"), tr("Time the object sets")},
      {HeaderType::MajorAxis, tr("Major Axis (\')"),
       tr("Object size along it's major axis in arc minutes")},
      {HeaderType::MinorAxis, tr("Minor Axis (\')"),
       tr("Object size along it's minor axis in arc minutes")},
      {HeaderType::BMag, tr("B-Mag"), tr("Object magnitude in the B band (blue)")},
      {HeaderType::VMag, tr("V-Mag"), tr("Object magnitude in the V band (visual)")},
  };
}

int AstroObjectTableModel::header_column(HeaderType type) const {
  const auto found = std::find_if(headers_.cbegin(), headers_.cend(),
                                  [type](const HeaderInfo info) { return info.type == type; });
  Expects(found != headers_.cend());
  return std::distance(headers_.cbegin(), found);
}

#include "ObservingSites.hpp"
#include "AstroHelper.hpp"
#include "ui_ObservingSites.h"
#include <QString>
#include <algorithm>
#include <gsl/gsl_util>

ObservingSites::ObservingSites(SiteRepository site_repository,
                               SettingsRepository settings_repository, QWidget *parent)
    : QDialog(parent), ui(new Ui::ObservingSites), site_repository_(std::move(site_repository)),
      settings_repository_(std::move(settings_repository)) {
  ui->setupUi(this);

  const int current_site_id = settings_repository_.fetch(1).current_site_id;
  update_site(site_repository_.fetch(current_site_id));

  const std::vector<ObservingSite> sites = site_repository_.fetch_all();
  ui->siteComboBox->addItem("-New Site-", -1);
  for (const auto &site : sites) {
    ui->siteComboBox->addItem(QString::fromStdString(site.name), site.id);
  }

  std::ptrdiff_t current_index =
      std::distance(sites.cbegin(), std::find_if(sites.cbegin(), sites.cend(),
                                                 [current_site_id](const auto &site) {
                                                   return site.id == current_site_id;
                                                 })) +
      1;
  if (static_cast<std::size_t>(current_index) > sites.size()) {
    current_index = 0;
  }
  ui->siteComboBox->setCurrentIndex(static_cast<int>(current_index));

  connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &ObservingSites::accept);
  connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &ObservingSites::reject);
}

ObservingSites::~ObservingSites() {
  delete ui;
}

void ObservingSites::accept() {
  observing_site_.name = ui->siteNameLineEdit->text().toStdString();
  observing_site_.latitude = parse_dd(ui->latLineEdit->text());
  observing_site_.longitude = parse_dd(ui->longLineEdit->text());
  observing_site_.height = ui->heightSpinBox->value();
  observing_site_.temperature = ui->tempSpinBox->value();
  observing_site_.humidity = ui->humiditySpinBox->value();
  observing_site_.pressure = ui->pressureSpinBox->value();
  observing_site_.utc_offset = hours_to_minutes(ui->utcOffsetDoubleSpinBox->value());

  bool update_success;
  if (observing_site_.id < 0) {
    update_success = site_repository_.create(observing_site_);
  } else {
    update_success = site_repository_.update(observing_site_);
  }
  if (update_success) {
    CurrentSettings settings = settings_repository_.fetch(1);
    settings.current_site_id = observing_site_.id;
    settings_repository_.update(settings);
  } else {
    qWarning().nospace() << "Failed to update/create site: " << observing_site_.name.c_str();
  }

  QDialog::accept();
}

ObservingSite ObservingSites::observing_site() const {
  return observing_site_;
}

double ObservingSites::parse_dd(const QString &dd) const {
  bool lat_long_parse;
  const double lat_long = dd.toDouble(&lat_long_parse);
  if (!lat_long_parse) {
    qInfo().nospace() << "Failed to parse lat/long: " << dd;
  }
  return AstroHelper::dd_to_rad(lat_long);
}

std::chrono::minutes ObservingSites::hours_to_minutes(double hours) const {
  return std::chrono::minutes(static_cast<std::int64_t>(hours * 60));
}

void ObservingSites::update_site(ObservingSite site) {
  observing_site_ = std::move(site);

  ui->siteNameLineEdit->setText(QString::fromStdString(observing_site_.name));
  ui->latLineEdit->setText(QString::number(AstroHelper::rad_to_dd(observing_site_.latitude)));
  ui->longLineEdit->setText(QString::number(AstroHelper::rad_to_dd(observing_site_.longitude)));
  ui->heightSpinBox->setValue(observing_site_.height);
  ui->tempSpinBox->setValue(observing_site_.temperature);
  ui->humiditySpinBox->setValue(observing_site_.humidity);
  ui->pressureSpinBox->setValue(observing_site_.pressure);
  ui->utcOffsetDoubleSpinBox->setValue(
      static_cast<double>(observing_site_.utc_offset.count() / 60.0));
}

void ObservingSites::on_siteComboBox_currentIndexChanged(int index) {
  const QVariant item_data = ui->siteComboBox->itemData(index);
  Expects(item_data.type() == QVariant::Int);
  const int site_id = item_data.toInt();
  if (site_id == -1) {
    update_site({});
  } else {
    update_site(site_repository_.fetch(site_id));
  }
}

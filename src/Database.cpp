#include "Database.hpp"
#include "AstroHelper.hpp"
#include "SkySurveyHelper.hpp"
#include <QDebug>
#include <QFile>
#include <QSqlDatabase>
#include <QSqlError>

Database::Database(const QString &db_driver, QString db_name, const QString &db_path)
    : db_name_(std::move(db_name)) {
  QSqlDatabase sql_database = QSqlDatabase::addDatabase(db_driver, db_name_);
  sql_database.setDatabaseName(db_path);
  if (!sql_database.open()) {
    throw std::runtime_error("Failed to open database connection");
  }
}

int Database::get_database_version() const {
  QSqlQuery query = get_query();
  query.setForwardOnly(true);
  if (!query.exec("select db_version from app_info")) {
    return 0;
  }
  if (!query.next()) {
    throw std::runtime_error("Couldn't find app_info record");
  }
  bool parse_success;
  const int db_version = query.value(0).toInt(&parse_success);
  if (!parse_success) {
    throw std::runtime_error("Failed to parse db_version");
  }
  return db_version;
}

void Database::db_upgrade() const {
  int db_version = get_database_version();
  if (db_version == database_version) {
    return;
  }
  if (db_version > database_version) {
    throw std::runtime_error("Database version is newer than expected, exiting");
  }

  qInfo().nospace() << "Database needs upgrade from v" << db_version << " to v" << database_version;

  // backup database (inside transaction to avoid corruption)
  {
    QSqlQuery query = get_query();

    if (!query.exec("BEGIN IMMEDIATE;")) {
      throw std::runtime_error("Failed to begin transaction");
    }
    const auto db_path = get_database().databaseName();
    const auto db_bak_path = db_path + ".bak";
    QFile::copy(db_path, db_bak_path);

    if (!query.exec("ROLLBACK;")) {
      throw std::runtime_error("Failed to end transaction");
    }
    qInfo().nospace() << "Backed up database prior to upgrade: " << db_bak_path;
  }

  for (++db_version; db_version <= database_version; ++db_version) {
    qInfo().nospace() << "Upgrading to v" << db_version << "...";
    execute_sql_script(QString{":/sql/%1.sql"}.arg(db_version));
    qInfo().nospace() << "Upgraded to v" << db_version << '.';
  }
  qInfo() << "Done.";
}

QSqlQuery Database::get_query() const {
  return QSqlQuery{get_database()};
}

void Database::begin_transaction() const {
  if (!get_database().transaction()) {
    throw std::runtime_error("Failed to begin transaction");
  }
}

void Database::end_transaction() const {
  if (!get_database().commit()) {
    throw std::runtime_error("Failed to commit changes");
  }
}

void Database::execute_sql_script(const QString &path) const {
  QSqlQuery query = get_query();
  QFile script_file{path};
  if (!script_file.open(QIODevice::ReadOnly)) {
    throw std::runtime_error("Failed to open SQL script");
  }
  QStringList queries = QTextStream(&script_file).readAll().split(';');

  begin_transaction();

  for (const QString &query_string : queries) {
    if (query_string.trimmed().isEmpty()) {
      continue;
    }
    if (!query.exec(query_string)) {
      qWarning().nospace() << "Failed to execute query: " << query.lastError();
      throw std::runtime_error("Failed to execute query");
    }
    query.finish();
  }

  end_transaction();
}

QSqlDatabase Database::get_database() const {
  return QSqlDatabase::database(db_name_);
}

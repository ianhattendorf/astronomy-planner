#include "AstroHelper.hpp"
#include <cmath>
#include <ctime>
#include <erfa.h>
#include <gsl/gsl_util>
#include <iomanip>
#include <iostream>
#include <limits>
#include <sstream>
#include <stdexcept>

std::string HoursMinutesSeconds::to_string(int decimal_places) const {
  std::stringstream ss;
  ss << sign << std::setfill('0') << std::setw(2) << hours << ':' << std::setfill('0')
     << std::setw(2) << minutes << ':' << std::setfill('0') << std::setw(2);
  ss << seconds;
  if (decimal_places > 0) {
    const std::string fraction_string = std::to_string(fraction);
    ss << '.'
       << fraction_string.substr(
              0, std::min(static_cast<std::size_t>(decimal_places), fraction_string.size()));
  } else if (decimal_places == -1) {
    ss << '.' << fraction;
  }
  return ss.str();
}

eraASTROM AstroHelper::get_astrom(const ObservingParameters &observing_parameters,
                                  std::chrono::seconds offset) {
  // TODO milliseconds via std::chrono?
  const std::time_t time_now =
      std::chrono::system_clock::to_time_t(std::chrono::system_clock::now() + offset);
  const std::tm *const tm_now = std::gmtime(&time_now);

  double utc1, utc2;
  if (const int rc = eraDtf2d("UTC", tm_now->tm_year + 1900, tm_now->tm_mon + 1, tm_now->tm_mday,
                              tm_now->tm_hour, tm_now->tm_min, tm_now->tm_sec, &utc1, &utc2);
      rc != 0) {
    throw std::runtime_error("Failed eraDtf2d, rc: " + std::to_string(rc));
  }

  // TODO check other params
  constexpr double dut1 = 0.07; // approx as of 2018/06/19, can we pull this from a web service?
  const double latitude = observing_parameters.latitude;
  const double longitude = observing_parameters.longitude;
  const double height_above_ellipsoid = observing_parameters.height;
  const double pressure_hpa = observing_parameters.pressure;
  const double current_temperature_celsius = observing_parameters.temperature;
  const double relative_humidity = observing_parameters.humidity;
  constexpr double light_wavelength_microns = 0.55;

  // Get star-independent astrometry parameters
  double eo;
  eraASTROM astrom;
  if (const int rc = eraApco13(utc1, utc2, dut1, longitude, latitude, height_above_ellipsoid, 0.0,
                               0.0, pressure_hpa, current_temperature_celsius, relative_humidity,
                               light_wavelength_microns, &astrom, &eo);
      rc != 0) {
    throw std::runtime_error("Failed eraApco13, rc: " + std::to_string(rc));
  }

  return astrom;
}

double AstroHelper::get_gast(std::chrono::seconds offset) {
  // TODO milliseconds via std::chrono?
  const std::time_t time_now =
      std::chrono::system_clock::to_time_t(std::chrono::system_clock::now() + offset);
  const std::tm *const tm_now = std::gmtime(&time_now);

  double utc1, utc2;
  if (const int rc = eraDtf2d("UTC", tm_now->tm_year + 1900, tm_now->tm_mon + 1, tm_now->tm_mday,
                              tm_now->tm_hour, tm_now->tm_min, tm_now->tm_sec, &utc1, &utc2);
      rc != 0) {
    throw std::runtime_error("Failed eraDtf2d, rc: " + std::to_string(rc));
  }

  // Should be UT1?
  return eraGst00b(utc1, utc2);
}

double AstroHelper::cos_ha_at_alt(double declination, double latitude, double altitude) {
  return (std::sin(altitude) - std::sin(declination) * std::sin(latitude)) /
         (std::cos(declination) * std::cos(latitude));
}

std::tuple<std::variant<double, RiseSet>, double, std::variant<double, RiseSet>>
AstroHelper::rise_transit_set_lst(double right_ascension, double declination, double latitude,
                                  double altitude) {
  const double lst_transit = right_ascension;

  // TODO Add fudge factor?
  if (std::abs(declination - latitude) > ERFA_DPI / 2) {
    // always below horizon
    return std::make_tuple(RiseSet::Never, lst_transit, RiseSet::Always);
  }
  if (std::abs(declination + latitude) > ERFA_DPI / 2) {
    // always above horizon
    return std::make_tuple(RiseSet::Always, lst_transit, RiseSet::Never);
  }

  const double ha_at_alt = AstroHelper::cos_ha_at_alt(declination, latitude, altitude);
  Expects(ha_at_alt < 1.0 && ha_at_alt > -1.0);

  const double lst_rise = rad_wrap_positive_two_pi(right_ascension - std::acos(ha_at_alt));
  const double lst_set = rad_wrap_positive_two_pi(right_ascension + std::acos(ha_at_alt));

  return std::make_tuple(lst_rise, lst_transit, lst_set);
}

double AstroHelper::transit_alt(double declination, double latitude) {
  return ERFA_DPI / 2.0 - std::abs(declination - latitude);
}

AstroObserved AstroHelper::icrs_to_observed(double ra, double dec, eraASTROM astrom) {

  // Transform ICRS to CIRS
  double ri, di;
  // TODO check other params
  eraAtciq(ra, dec, 0.0, 0.0, 0.0, 0.0, &astrom, &ri, &di);

  // Transform CIRS to observed
  AstroObserved observed{};
  eraAtioq(ri, di, &astrom, &observed.azimuth, &observed.zenith, &observed.hour_angle,
           &observed.dec, &observed.ra);

  return observed;
}

DegreesMinutesSeconds AstroHelper::rad_to_dms(double rad) {
  // Test for rounding at ~2pi?
  char sign;
  int idmsf[4];
  // NOLINTNEXTLINE(hicpp-no-array-decay)
  eraA2af(frac_resolution, rad, &sign, idmsf);
  return DegreesMinutesSeconds{sign, idmsf[0], idmsf[1], idmsf[2], idmsf[3]};
}

HoursMinutesSeconds AstroHelper::rad_to_hms(double rad) {
  // Test for rounding at ~2pi?
  char sign;
  int ihmsf[4];
  // NOLINTNEXTLINE(hicpp-no-array-decay)
  eraA2tf(frac_resolution, rad, &sign, ihmsf);
  return HoursMinutesSeconds{sign, ihmsf[0], ihmsf[1], ihmsf[2], ihmsf[3]};
}

double AstroHelper::rad_wrap_negative_pi(double rad) {
  return eraAnpm(rad);
}

double AstroHelper::rad_wrap_positive_two_pi(double rad) {
  return eraAnp(rad);
}

double AstroHelper::hms_to_rad(char sign, int hours, int minutes, int seconds) {
  double rad;
  if (const int rc = eraTf2a(sign, hours, minutes, seconds, &rad); rc != 0) {
    throw std::runtime_error("Failed to convert hms to rad");
  }
  return rad;
}

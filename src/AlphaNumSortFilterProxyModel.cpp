#include "AlphaNumSortFilterProxyModel.hpp"
#include "alphanum.hpp"

AlphaNumSortFilterProxyModel::AlphaNumSortFilterProxyModel(QObject *parent)
    : QSortFilterProxyModel(parent) {}

bool AlphaNumSortFilterProxyModel::lessThan(const QModelIndex &source_left,
                                            const QModelIndex &source_right) const {
  const QVariant left_data = sourceModel()->data(source_left);
  const QVariant right_data = sourceModel()->data(source_right);

  if (left_data.type() != QVariant::String || right_data.type() != QVariant::String) {
    return QSortFilterProxyModel::lessThan(source_left, source_right);
  }

  return doj::alphanum_comp(left_data.toString().toStdString(),
                            right_data.toString().toStdString()) < 0;
}

#include "ClickableLabel.hpp"
#include <QDebug>
#include <QResizeEvent>

ClickableLabel::ClickableLabel(QWidget *parent, Qt::WindowFlags flags) : QLabel(parent, flags) {}

void ClickableLabel::setPixmapScaled(const QPixmap &pixmap) {
  if (pixmap.isNull()) {
    return;
  }
  setPixmap(pixmap.scaled(size().width(), size().height(), Qt::KeepAspectRatio,
                          Qt::SmoothTransformation));
}

void ClickableLabel::mousePressEvent(QMouseEvent * /*event*/) {
  emit clicked();
}

void ClickableLabel::resizeEvent(QResizeEvent *event) {
  if (event->oldSize() == event->size()) {
    qInfo() << "New and old sizes match, not emitting resized event";
    return;
  }
  emit resized(event->size());
}

#include "AstroObjectRepository.hpp"
#include "AstroHelper.hpp"
#include <sstream>

AstroObjectRepository::AstroObjectRepository(Database database)
    : BaseRepository(std::move(database)) {}

std::vector<AstroObject> AstroObjectRepository::fetch_all() const {
  QSqlQuery query = get_query();
  if (!query.exec(QString{"select objects.id, objects.object_id, sky_survey.name, "
                          "%1 from objects left outer join sky_survey on sky_survey.id = "
                          "objects.preferred_survey_image %2"}
                      .arg(astro_object_coalesce_, astro_object_join_))) {
    throw std::runtime_error("get_objects failed to exec query");
  }
  std::vector<AstroObject> objects;
  while (query.next()) {
    const QVariant preferred_survey_image = query.value(2);
    const std::optional<SkySurvey> preferred_survey_image_opt =
        preferred_survey_image.isNull()
            ? std::nullopt
            : std::optional<SkySurvey>{SkySurveyHelper::get_survey_from_name_short(
                  preferred_survey_image.toString().toUtf8().constData())};
    objects.emplace_back(AstroObject{
        query.value(0).toInt(), query.value(1).toString(), preferred_survey_image_opt,
        query.value(3).toString(), AstroHelper::dd_to_rad(query.value(4).toDouble()),
        AstroHelper::dd_to_rad(query.value(5).toDouble()), query.value(6).toDouble(),
        query.value(7).toDouble(), query.value(8).toDouble(), query.value(9).toDouble()});
  }
  return objects;
}

bool AstroObjectRepository::create(AstroObject &astro_object) const {
  QSqlQuery query = get_query();
  query.prepare("insert into objects (object_id, date_created) values (?, ?)");
  query.addBindValue(astro_object.object_id);
  query.addBindValue(QDateTime::currentDateTimeUtc().toString(Qt::ISODate));
  if (query.exec()) {
    astro_object.id = query.lastInsertId().toInt();
    return true;
  }
  return false;
}

bool AstroObjectRepository::update(const AstroObject &astro_object) const {
  QSqlQuery query = get_query();
  query.prepare(
      "update objects set object_id = ?, preferred_survey_image = (select sky_survey.id from "
      "sky_survey where sky_survey.name = ?) where objects.id = ?");
  query.addBindValue(astro_object.object_id);
  const QVariant survey_name = astro_object.preferred_survey_image.has_value()
                                   ? QString::fromStdString(SkySurveyHelper::get_survey_name_short(
                                         astro_object.preferred_survey_image.value()))
                                   : QVariant{};
  query.addBindValue(survey_name);
  query.addBindValue(astro_object.id);
  return query.exec();
}

bool AstroObjectRepository::del(const AstroObject &astro_object) const {
  QSqlQuery query = get_query();
  query.prepare("delete from objects where id = ?");
  query.addBindValue(astro_object.id);
  return query.exec();
}

void AstroObjectRepository::build_coalesce_single(std::ostream &os, int count,
                                                  std::string_view field_name) const {
  os << "coalesce(";
  std::string separator;
  for (int i = 1; i <= count; ++i) {
    os << separator;
    os << 'c';
    os << i;
    os << '.';
    os << field_name;

    separator = ", ";
  }
  os << ")";
}

std::string AstroObjectRepository::build_coalesce_multiple(
    int count, std::initializer_list<std::string_view> fields) const {
  std::stringstream ss;
  std::string separator;
  for (const auto field : fields) {
    ss << separator;
    build_coalesce_single(ss, count, field);
    separator = ", ";
  }
  return ss.str();
}

void AstroObjectRepository::fill_astro_object_data(AstroObject &astro_object) const {
  QSqlQuery query = get_query();
  query.prepare(QString{"select %1 from (select ? as object_id) as objects %2"}.arg(
      astro_object_coalesce_, astro_object_join_));
  query.addBindValue(astro_object.object_id);
  query.exec();
  if (!query.next()) {
    throw std::runtime_error("Failed to fetch query");
  }
  astro_object.common_names = query.value(0).toString();
  astro_object.ra = AstroHelper::dd_to_rad(query.value(1).toDouble());
  astro_object.dec = AstroHelper::dd_to_rad(query.value(2).toDouble());
  astro_object.maj_ax = query.value(3).toDouble();
  astro_object.min_ax = query.value(4).toDouble();
  astro_object.b_mag = query.value(5).toDouble();
  astro_object.v_mag = query.value(6).toDouble();
}

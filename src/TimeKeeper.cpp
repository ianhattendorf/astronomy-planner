#include "TimeKeeper.hpp"
#include <QDebug>

TimeKeeper::TimeKeeper(QObject *parent) : QObject(parent) {}

QDateTime TimeKeeper::local_time() const {
  return QDateTime::currentDateTimeUtc().toOffsetFromUtc(utc_offset_.count());
}

QDateTime TimeKeeper::local_offset_time() const {
  return local_time().addSecs(offset_.count());
}

std::chrono::seconds TimeKeeper::offset() const {
  return offset_;
}

void TimeKeeper::set_offset(std::chrono::seconds offset) {
  offset_ = offset;
  qDebug().nospace() << "offset: " << offset_.count();
  emit offset_changed();
}

void TimeKeeper::set_utc_offset(std::chrono::seconds utc_offset) {
  utc_offset_ = utc_offset;
  qDebug().nospace() << "utc_offset: " << utc_offset_.count();
  emit utc_offset_changed();
}

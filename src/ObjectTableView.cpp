#include "ObjectTableView.hpp"
#include <QApplication>
#include <QClipboard>
#include <QKeyEvent>

ObjectTableView::ObjectTableView(QWidget *parent) : QTableView(parent) {}

void ObjectTableView::keyPressEvent(QKeyEvent *event) {
  if (event->matches(QKeySequence::Copy) && !selectedIndexes().isEmpty()) {
    QString text;
    const QItemSelectionRange range = selectionModel()->selection().first();
    QString separator;
    for (auto i = range.top(); i <= range.bottom(); ++i) {
      text += separator;
      separator = '\n';

      QStringList row_contents;
      for (auto j = range.left(); j <= range.right(); ++j) {
        row_contents.append(model()->index(i, j).data().toString());
      }
      text += row_contents.join('\t');
    }
    QApplication::clipboard()->setText(text);
  } else {
    QTableView::keyPressEvent(event);
  }
}

#include "SettingsRepository.hpp"

SettingsRepository::SettingsRepository(Database database) : BaseRepository(std::move(database)) {}

CurrentSettings SettingsRepository::fetch(int id) const {
  QSqlQuery query = get_query();
  query.prepare("select id, current_site from current_settings where id = ?");
  query.addBindValue(id);
  if (!query.exec()) {
    throw std::runtime_error("get_current_settings exec failed");
  }
  if (!query.next()) {
    throw std::runtime_error("no settings found");
  }

  CurrentSettings settings{};
  settings.id = query.value(0).toInt();
  settings.current_site_id = query.value(1).toInt();
  return settings;
}

bool SettingsRepository::update(const CurrentSettings &settings) const {
  QSqlQuery query = get_query();
  query.prepare("update current_settings set current_site = ? where id = ?");
  query.addBindValue(settings.current_site_id);
  query.addBindValue(settings.id);
  return query.exec();
}

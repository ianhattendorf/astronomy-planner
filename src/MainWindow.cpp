#include "MainWindow.hpp"
#include "AlphaNumSortFilterProxyModel.hpp"
#include "AstroHelper.hpp"
#include "AstroObjectTableModel.hpp"
#include "AstroStyledItemDelegate.hpp"
#include "ClickableLabel.hpp"
#include "Database.hpp"
#include "DownloadManager.hpp"
#include "ObservingSites.hpp"
#include "SettingsRepository.hpp"
#include "TimeKeeper.hpp"
#include "ui_MainWindow.h"
#include <QCloseEvent>
#include <QMessageBox>
#include <QPixmap>
#include <cmath>
#include <csv.h>
#include <erfa.h>
#include <exception>
#include <gsl/gsl_util>
#include <regex>

struct SqlData {
  QSqlQuery query;
  int field = 0;
  std::regex ra_regex;
  std::regex dec_regex;
  std::smatch ra_match;
  std::smatch dec_match;
};

struct ImageComboBoxData {
  QString name;
  QString path;
  SkySurvey survey{};
};

// NOLINTNEXTLINE(cppcoreguidelines-pro-type-reinterpret-cast)
Q_DECLARE_METATYPE(ImageComboBoxData)

ImageQueueInfo::ImageQueueInfo(QUrl url, QString path, QString name, SkySurvey survey)
    : url_(std::move(url)), path_(std::move(path)), name_(std::move(name)), survey_(survey) {}

ImageDownloadInfo::ImageDownloadInfo(QNetworkReply *reply, const ImageQueueInfo &info)
    : reply_(reply), path_(info.path()), name_(info.name()), survey_(info.survey()) {}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow),
      catalog_download_manager_(new DownloadManager(this)),
      images_download_manager_(new DownloadManager(this)),
      obect_image_label_(new ClickableLabel(this, Qt::Window)) {
  ui->setupUi(this);

  /* Setup actions */
  QAction *sites_action = new QAction(tr("Edit &Sites"), this);
  connect(sites_action, &QAction::triggered, this, &MainWindow::edit_sites);

  QAction *download_catalog_action = new QAction(tr("&Download Catalog"), this);
  connect(download_catalog_action, &QAction::triggered, this, &MainWindow::download_catalog_start);
  connect(catalog_download_manager_, &DownloadManager::finished, this,
          &MainWindow::download_catalog_finished);

  QAction *download_images_action = new QAction(tr("Download &Images"), this);
  connect(download_images_action, &QAction::triggered, this, &MainWindow::download_images_start);
  connect(images_download_manager_, &DownloadManager::finished, this,
          &MainWindow::download_images_finished);

  QAction *exit_action = new QAction(tr("&Quit"), this);
  // NOLINTNEXTLINE(misc-suspicious-enum-usage)
  exit_action->setShortcut(Qt::CTRL | Qt::Key_Q);
  connect(exit_action, &QAction::triggered, this, &MainWindow::close);

  auto fileMenu = menuBar()->addMenu(tr("&File"));
  fileMenu->addAction(sites_action);
  fileMenu->addAction(download_catalog_action);
  fileMenu->addAction(download_images_action);
  fileMenu->addAction(exit_action);

  /* Setup file paths */
  if (!QDir().mkpath(get_settings_path())) {
    throw std::runtime_error("Failed to create settings directory: " +
                             get_settings_path().toStdString());
  }

  if (!QDir().mkpath(get_dss_path())) {
    throw std::runtime_error("Failed to create dss directory: " + get_dss_path().toStdString());
  }

  if (!QDir().mkpath(get_sdss_path())) {
    throw std::runtime_error("Failed to create sdss directory: " + get_sdss_path().toStdString());
  }

  /* Setup database */
  database_ = std::make_unique<Database>("QSQLITE", "astro.db", get_db_path());
  database_->db_upgrade();

  settings_repository_ = std::make_unique<SettingsRepository>(*database_);
  site_repository_ = std::make_unique<SiteRepository>(*database_);

  current_settings_ = settings_repository_->fetch(1);

  /* Setup widgets */
  connect(ui->objectImageLabel, &ClickableLabel::clicked, this, &MainWindow::image_clicked);

  obect_image_label_->hide();
  connect(obect_image_label_, &ClickableLabel::resized, [this](const QSize & /*size*/) {
    if (object_preview_image_ == nullptr) {
      return;
    }
    obect_image_label_->setPixmapScaled(*object_preview_image_);
  });
  connect(obect_image_label_, &ClickableLabel::clicked, obect_image_label_, &ClickableLabel::close);

  time_keeper_ = new TimeKeeper{this};

  model_ = new AstroObjectTableModel{this, AstroObjectRepository{*database_}, time_keeper_};
  auto *const proxy_model = new AlphaNumSortFilterProxyModel{this};
  proxy_model->setSourceModel(model_);
  ui->tableView->setModel(proxy_model);
  // TODO don't use raw column index
  ui->tableView->setItemDelegateForColumn(
      3, new AstroStyledItemDelegate{AstroStyleType::HMS24, this}); // ra
  ui->tableView->setItemDelegateForColumn(
      4, new AstroStyledItemDelegate{AstroStyleType::DMS90PosNeg, this}); // dec
  ui->tableView->setItemDelegateForColumn(
      5, new AstroStyledItemDelegate{AstroStyleType::D360, this}); // az
  ui->tableView->setItemDelegateForColumn(
      6, new AstroStyledItemDelegate{AstroStyleType::D90PosNeg, this}); // alt
  ui->tableView->setItemDelegateForColumn(
      8, new AstroStyledItemDelegate{AstroStyleType::HMS12PosNeg, this}); // ha
  ui->tableView->setItemDelegateForColumn(
      11, new AstroStyledItemDelegate{AstroStyleType::D90PosNeg, this}); // transit alt
  ui->tableView->setColumnHidden(0, true);
  ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
  ui->tableView->setSortingEnabled(true);
  connect(ui->tableView->selectionModel(), &QItemSelectionModel::currentRowChanged, this,
          &MainWindow::table_row_changed);

  // After model set up
  set_observing_site(site_repository_->fetch(current_settings_.current_site_id));

  auto *const timer = new QTimer{this};
  connect(timer, &QTimer::timeout, this, [this, timer]() {
    const auto now = time_keeper_->local_offset_time();

    // TODO if user changes time while paused, time won't update until pause is toggled
    if (!ui->timePausedCheckBox->isChecked()) {
      const double greenwich_sidereal = AstroHelper::get_gast(time_keeper_->offset());
      const double local_sidereal = greenwich_sidereal + observing_site_.longitude;
      ui->gstLcdNumber->display(
          QString::fromStdString(AstroHelper::rad_to_hms(greenwich_sidereal).to_string(0)));
      ui->lstLcdNumber->display(
          QString::fromStdString(AstroHelper::rad_to_hms(local_sidereal).to_string(0)));

      ui->ltLcdNumber->display(now.toString("hh:mm:ss"));

      if (focusWidget() != ui->dateTimeEdit) {
        ui->dateTimeEdit->setDateTime(now);
      }

      model_->tick();
    }

    // Coarse timer can be +/- 5%
    timer->start(1075 - now.time().msec());
  });
  timer->setTimerType(Qt::CoarseTimer);
  timer->start(1000);
}

MainWindow::~MainWindow() {
  delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event) {
  if (model_->is_dirty()) {
    QMessageBox box;
    box.setText(tr("You have unsaved changes"));
    box.setInformativeText(tr("Are you sure you want to exit?"));
    box.setIcon(QMessageBox::Warning);
    box.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    box.setDefaultButton(QMessageBox::No);
    const int ret = box.exec();
    if (ret == QMessageBox::Yes) {
      event->accept();
    } else {
      event->ignore();
    }
  } else {
    event->accept();
  }
}

void MainWindow::on_sqlRevertButton_clicked() {
  model_->revertAll();
}

void MainWindow::on_sqlSubmitButton_clicked() {
  qDebug().nospace() << "submit: " << model_->submitAll();
}

void MainWindow::on_sqlNewRowButton_clicked() {
  ui->tableView->model()->insertRow(ui->tableView->model()->rowCount());
  const auto index = qobject_cast<QAbstractProxyModel *>(ui->tableView->model())
                         ->mapFromSource(model_->index(model_->rowCount(QModelIndex()) - 1, 1));
  ui->tableView->scrollTo(index);
  ui->tableView->selectionModel()->setCurrentIndex(index, QItemSelectionModel::Select);
  ui->tableView->edit(index);
  ui->tableView->selectionModel()->clearSelection();
}

void MainWindow::on_sqlDeleteRowsButton_clicked() {
  const auto selection_model = ui->tableView->selectionModel();
  if (!selection_model->hasSelection()) {
    qDebug() << "No selection";
    return;
  }

  const auto selected_rows = selection_model->selectedRows();
  for (const auto &selected_row : selected_rows) {
    ui->tableView->model()->removeRows(selected_row.row(), 1);
  }
}

void MainWindow::on_objectImageComboBox_currentIndexChanged(int index) {
  if (index == -1) {
    object_preview_image_.reset();
    ui->objectImageLabel->setPixmap(QPixmap{});
    obect_image_label_->setPixmap(QPixmap{});
    return;
  }

  const QVariant image_data_variant = ui->objectImageComboBox->itemData(index);
  Expects(image_data_variant.userType() == qMetaTypeId<ImageComboBoxData>());
  const ImageComboBoxData image_data = image_data_variant.value<ImageComboBoxData>();
  object_preview_image_ = std::make_unique<QPixmap>(image_data.path);
  ui->objectImageLabel->setPixmapScaled(*object_preview_image_);
  obect_image_label_->setPixmapScaled(*object_preview_image_);
}

void MainWindow::on_objectImageComboBox_activated(int index) {
  const QVariant image_data_variant = ui->objectImageComboBox->itemData(index);
  Expects(image_data_variant.userType() == qMetaTypeId<ImageComboBoxData>());
  const ImageComboBoxData image_data = image_data_variant.value<ImageComboBoxData>();

  qDebug().nospace() << "Updating image preference: "
                     << SkySurveyHelper::get_survey_name_short(image_data.survey).c_str();
  ui->tableView->model()->setData(ui->tableView->currentIndex(),
                                  QVariant::fromValue(image_data.survey), Qt::UserRole);
}

void MainWindow::on_dateTimeEdit_editingFinished() {
  const QDateTime date_time = ui->dateTimeEdit->dateTime();
  time_keeper_->set_offset(std::chrono::seconds(-date_time.secsTo(time_keeper_->local_time())));
}

void MainWindow::on_pushButton_clicked() {
  ui->dateTimeEdit->setDateTime(time_keeper_->local_time());
  emit ui->dateTimeEdit->editingFinished();
}

void MainWindow::download_catalog_start() {
  // TODO provide visual progress
  if (catalog_reply_ != nullptr) {
    qInfo() << "Catalog download already in progress";
    return;
  }
  catalog_reply_ = catalog_download_manager_->do_download(
      QUrl{"https://raw.githubusercontent.com/mattiaverga/OpenNGC/master/NGC.csv"});
}

void MainWindow::download_catalog_finished(QNetworkReply *reply) {
  if (catalog_reply_ != reply) {
    qDebug() << "Catalog reply finished, but didn't match";
    return;
  }

  parse_and_save_catalog(reply->readAll());

  reply->deleteLater();
  catalog_reply_ = nullptr;

  QMessageBox box;
  box.setText(tr("Catalog download finished."));
  box.setIcon(QMessageBox::Information);
  box.exec();
}

void MainWindow::download_images_start() {
  for (int i = 0; i < ui->tableView->model()->rowCount(); ++i) {
    const QString name =
        ui->tableView->model()->data(ui->tableView->model()->index(i, 1)).toString();

    bool ra_parse, dec_parse;
    double ra =
        ui->tableView->model()->data(ui->tableView->model()->index(i, 3)).toDouble(&ra_parse);
    double dec =
        ui->tableView->model()->data(ui->tableView->model()->index(i, 4)).toDouble(&dec_parse);
    if (!ra_parse || !dec_parse) {
      qInfo().nospace() << "Skipping image download for row " << i << " with name [" << name
                        << "], missing ra or dec";
      continue;
    }
    ra = AstroHelper::rad_to_dd(ra);
    dec = AstroHelper::rad_to_dd(dec);

    bool size_parse;
    double size =
        ui->tableView->model()->data(ui->tableView->model()->index(i, 5)).toDouble(&size_parse);
    size *= 1.1;
    if (!size_parse || size < default_image_size) {
      size = default_image_size;
    }
    size /= 60.0; // arcmin to degrees

    for (const SkySurvey survey : surveys_) {
      const QString image_path = get_image_path(survey, name);
      if (QFileInfo::exists(image_path)) {
        continue;
      }

      image_queue_.enqueue(ImageQueueInfo{QUrl{QString::fromStdString(SkySurveyHelper::get_url(
                                              survey, ra, dec, size, image_num_pixels))},
                                          image_path, name, survey});
    }
  }

  qInfo().nospace() << "Enqueued " << image_queue_.size() << " images to download";
  for (int i = 0; i < max_concurrent_downloads; ++i) {
    download_next_queued_image();
  }
}

void MainWindow::download_images_finished(QNetworkReply *reply) {
  const auto found_download =
      std::find_if(image_replies_.begin(), image_replies_.end(),
                   [reply](const auto &info) { return info.reply() == reply; });
  Expects(found_download != image_replies_.cend());

  if (reply->error() != QNetworkReply::NoError) {
    qInfo().nospace() << "Download failed ["
                      << SkySurveyHelper::get_survey_name_short(found_download->survey()).c_str()
                      << "]: " << found_download->name();
  } else {
    qInfo().nospace() << "Download finished ["
                      << SkySurveyHelper::get_survey_name_short(found_download->survey()).c_str()
                      << "]: " << found_download->name();
    QFile file{found_download->path()};
    file.open(QIODevice::WriteOnly);
    file.write(reply->readAll());
  }

  reply->deleteLater();
  image_replies_.erase(found_download);
  download_next_queued_image();
  if (image_replies_.empty()) {
    qInfo() << "Finished downloading images.";
    QMessageBox box;
    box.setText(tr("Finished downloading images."));
    box.setIcon(QMessageBox::Information);
    box.exec();
  }
}

void MainWindow::image_clicked() {
  obect_image_label_->show();
}

void MainWindow::table_row_changed(const QModelIndex &current, const QModelIndex & /*previous*/) {
  ui->objectImageComboBox->clear();

  const QString name =
      ui->tableView->model()->data(ui->tableView->model()->index(current.row(), 1)).toString();
  std::vector<SkySurvey> available_surveys;
  for (const SkySurvey survey : surveys_) {
    const QString image_path = get_image_path(survey, name);
    if (!QFileInfo::exists(image_path)) {
      continue;
    }
    const QString image_name = QString{"%1 - %2"}.arg(
        QString::fromStdString(SkySurveyHelper::get_survey_name_short(survey)), name);
    ui->objectImageComboBox->addItem(
        image_name, QVariant::fromValue(ImageComboBoxData{name, image_path, survey}));
    available_surveys.emplace_back(survey);
  }

  const QVariant preferred_survey = ui->tableView->model()->data(current, Qt::UserRole);
  if (!preferred_survey.isNull()) {
    Expects(preferred_survey.userType() == qMetaTypeId<SkySurvey>());
    const std::ptrdiff_t preferred_index = std::distance(
        available_surveys.cbegin(), std::find(available_surveys.cbegin(), available_surveys.cend(),
                                              preferred_survey.value<SkySurvey>()));
    if (preferred_index >= static_cast<std::ptrdiff_t>(available_surveys.size())) {
      qInfo() << "Preferred survey image not available";
      return;
    }

    ui->objectImageComboBox->setCurrentIndex(preferred_index);
  }
}

void MainWindow::edit_sites() {
  ObservingSites observing_sites_window{*site_repository_, *settings_repository_};
  if (observing_sites_window.exec() == QDialog::Accepted) {
    const ObservingSite new_site = observing_sites_window.observing_site();
    if (!site_repository_->update(new_site)) {
      throw std::runtime_error("Failed to update observing site");
    }
    set_observing_site(new_site);
  }
}

void MainWindow::download_next_queued_image() {
  if (image_queue_.isEmpty()) {
    return;
  }

  const ImageQueueInfo image_to_download = image_queue_.dequeue();
  qInfo().nospace() << "Downloading ["
                    << SkySurveyHelper::get_survey_name_short(image_to_download.survey()).c_str()
                    << "]: " << image_to_download.name();
  auto reply = images_download_manager_->do_download(image_to_download.url());
  image_replies_.emplace_back(reply, image_to_download);
}

void MainWindow::set_observing_site(ObservingSite observing_site) {
  observing_site_ = std::move(observing_site);
  time_keeper_->set_utc_offset(observing_site_.utc_offset);

  ObservingParameters params{};
  params.latitude = observing_site_.latitude;
  params.longitude = observing_site_.longitude;
  params.height = observing_site_.height;
  params.humidity = observing_site_.humidity / 100.0;
  params.pressure = observing_site_.pressure;
  params.temperature = observing_site_.temperature;
  model_->set_observing_parameters(std::move(params));
}

void MainWindow::parse_and_save_catalog(const QByteArray &catalog_data) const {
  Expects(catalog_data.size() >= 0);
  std::string_view data_view{catalog_data.constData(),
                             static_cast<std::size_t>(catalog_data.size())};

  // C++20 std::string_view::starts_with
  if (data_view.size() < catalog_header_.size() ||
      data_view.compare(0, catalog_header_.size(), catalog_header_) != 0) {
    throw std::runtime_error("Downloaded data header doesn't match expected header");
  }
  // Move data_view forward
  data_view.remove_prefix(catalog_header_.size());

  SqlData sql_data{database_->get_query(),
                   0,
                   std::regex{R"((\d{2}):(\d{2}):(\d{2}(?:\.\d{1,2})?))"},
                   std::regex{R"(([+-])(\d{2}):(\d{2}):(\d{2}(?:\.\d{1,2})?))"},
                   std::smatch{},
                   std::smatch{}};
  // CSV fields: 0-9, 18-20, 23-25
  // int: 18-20
  // number: 5-6, 8-9
  sql_data.query.prepare(R"(insert into catalog
                           (name, type, ra, dec, const, maj_ax, min_ax, pos_ang,
                           b_mag, v_mag, m_id, ngc_id, ic_id, common_names,
                           ned_notes, open_ngc_notes)
                           values
                           (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?))");

  // NOLINTNEXTLINE(cppcoreguidelines-pro-type-member-init, hicpp-member-init)
  csv_parser parser;
  if (const int rc = csv_init(&parser, 0); rc != 0) {
    throw std::runtime_error("Failed to initialize csv parser, rc: " + std::to_string(rc));
  }
  csv_set_delim(&parser, ';');
  // NOLINTNEXTLINE(hicpp-signed-bitwise)
  csv_set_opts(&parser, CSV_STRICT | CSV_APPEND_NULL);

  database_->begin_transaction();

  qInfo() << "Deleting old catalog data (if it exists)";

  if (!database_->get_query().exec("delete from catalog")) {
    throw std::runtime_error("Failed to delete old catalog data");
  }

  qInfo() << "Parsing & insterting catalog data...";

  const size_t csv_bytes_parsed = csv_parse(
      &parser, data_view.data(), data_view.size(),
      [](void *field_chars, size_t /*field_length*/, void *cb_data) {
        // field callback

        auto sql_cb_data = static_cast<SqlData *>(cb_data);
        const int field = sql_cb_data->field;
        QString field_string{static_cast<char *>(field_chars)};
        if ((field >= 0 && field <= 9) || (field >= 18 && field <= 20) ||
            (field >= 23 && field <= 25)) {
          if (field == 0) {
            // name, strip leading 0s
            if (field_string.startsWith("NGC0") || field_string.startsWith("IC0")) {
              const int start = field_string.indexOf('0');
              int num_zeroes = 0;
              for (int i = start; i < field_string.size(); ++i) {
                if (field_string.at(i) != '0') {
                  break;
                }
                ++num_zeroes;
              }
              field_string.remove(start, num_zeroes);
              sql_cb_data->query.addBindValue(field_string);
            } else {
              sql_cb_data->query.addBindValue(field_string);
            }
          } else if (field == 2) {
            // RA
            const std::string ra = field_string.toStdString();
            if (std::regex_match(ra, sql_cb_data->ra_match, sql_cb_data->ra_regex)) {
              double ra_rad;
              if (const int ret = eraTf2a('+', std::stoi(sql_cb_data->ra_match[1].str()),
                                          std::stoi(sql_cb_data->ra_match[2].str()),
                                          std::stod(sql_cb_data->ra_match[3].str()), &ra_rad);
                  ret != 0) {
                throw std::runtime_error("Failed to convert RA to rad, rc = " +
                                         std::to_string(ret));
              }
              sql_cb_data->query.addBindValue(ra_rad * 180 / M_PI);
            } else {
              sql_cb_data->query.addBindValue(QVariant{QVariant::Double});
            }
          } else if (field == 3) {
            // Dec
            const std::string dec = field_string.toStdString();
            if (std::regex_match(dec, sql_cb_data->dec_match, sql_cb_data->dec_regex)) {
              double dec_rad;
              if (const int ret = eraAf2a(dec[0], std::stoi(sql_cb_data->dec_match[2].str()),
                                          std::stoi(sql_cb_data->dec_match[3].str()),
                                          std::stod(sql_cb_data->dec_match[4].str()), &dec_rad);
                  ret != 0) {
                throw std::runtime_error("Failed to convert dec to rad, rc = " +
                                         std::to_string(ret));
              }
              sql_cb_data->query.addBindValue(dec_rad * 180 / M_PI);
            } else {
              sql_cb_data->query.addBindValue(QVariant{QVariant::Double});
            }
          } else if (field == 5 || field == 6 || field == 8 || field == 9) {
            bool parse_success;
            const double field_double = field_string.toDouble(&parse_success);
            if (parse_success) {
              sql_cb_data->query.addBindValue(field_double);
            } else {
              sql_cb_data->query.addBindValue(QVariant{QVariant::Double});
            }
          } else if (field >= 18 && field <= 20) {
            bool parse_success;
            const int field_int = field_string.toInt(&parse_success);
            if (parse_success) {
              sql_cb_data->query.addBindValue(field_int);
            } else {
              sql_cb_data->query.addBindValue(QVariant{QVariant::Int});
            }
          } else {
            sql_cb_data->query.addBindValue(field_string);
          }
        }
        sql_cb_data->field++;
      },
      [](int /*c*/, void *cb_data) {
        // end of record callback, c = char that caused record to end or -1 if from
        // csv_fini
        auto sql_cb_data = static_cast<SqlData *>(cb_data);
        if (!sql_cb_data->query.exec()) {
          qWarning() << sql_cb_data->query.lastError();
          throw std::runtime_error("SQL exec error");
        }

        sql_cb_data->field = 0;
      },
      &sql_data);
  csv_free(&parser);
  if (csv_bytes_parsed != data_view.size()) {
    throw std::runtime_error("Only parsed " + std::to_string(csv_bytes_parsed) + " out of " +
                             std::to_string(data_view.size()) + " bytes");
  }
  database_->end_transaction();
  qInfo() << "Done parsing & inserting.";
}

QString MainWindow::get_settings_path() const {
  return QStandardPaths::writableLocation(QStandardPaths::AppLocalDataLocation);
}

QString MainWindow::get_db_path() const {
  return get_settings_path() + "/astro.db";
}

QString MainWindow::get_dss_path() const {
  return get_settings_path() + "/dss";
}

QString MainWindow::get_sdss_path() const {
  return get_settings_path() + "/sdss";
}

QString MainWindow::get_image_path(SkySurvey survey, const QString &name) const {
  QString image_path;
  switch (survey) {
  case SkySurvey::DSS:
    image_path = get_dss_path();
    break;
  case SkySurvey::SDSS:
    image_path = get_sdss_path();
    break;
  default:
    throw std::runtime_error("Unknown survey");
  }
  return QString{"%1/%2.jpeg"}.arg(image_path, name);
}

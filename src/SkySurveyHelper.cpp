#include "SkySurveyHelper.hpp"

#include <exception>
#include <sstream>

std::string SkySurveyHelper::get_url(SkySurvey survey, double ra, double dec, double size,
                                     int pixels) {
  std::stringstream ss;
  switch (survey) {
  case SkySurvey::DSS: {
    ss << "https://skyview.gsfc.nasa.gov/current/cgi/runquery.pl?Survey=dss&position=" << ra << ","
       << dec << "&Return=JPG&Pixels=" << pixels << "&Size=" << size;
    return ss.str();
  }
  case SkySurvey::SDSS: {
    const double size_arcsec_pixel = size * 3600 / pixels;
    ss << "http://skyserver.sdss.org/dr14/SkyServerWS/ImgCutout/getjpeg?ra=" << ra << "&dec=" << dec
       << "&width=" << pixels << "&height=" << pixels << "&scale=" << size_arcsec_pixel;
    return ss.str();
  }
  }
  throw std::runtime_error("Unknown survey");
}

std::string SkySurveyHelper::get_survey_name_short(SkySurvey survey) {
  switch (survey) {
  case SkySurvey::DSS:
    return "DSS";
  case SkySurvey::SDSS:
    return "SDSS";
  }
  throw std::runtime_error("Unknown survey");
}

SkySurvey SkySurveyHelper::get_survey_from_name_short(std::string_view name) {
  if (name == "DSS") {
    return SkySurvey::DSS;
  }
  if (name == "SDSS") {
    return SkySurvey::SDSS;
  }
  throw std::runtime_error("Unknown survey");
}

#include "SiteRepository.hpp"

SiteRepository::SiteRepository(Database database) : BaseRepository(std::move(database)) {}

ObservingSite SiteRepository::fetch(int id) const {
  QSqlQuery query = get_query();
  query.prepare(QString{"select id, name, latitude, longitude, height, temperature, humidity, "
                        "pressure, utc_offset from sites where id = ?"});
  query.addBindValue(id);
  if (!query.exec()) {
    throw std::runtime_error("get_observing_site exec failed");
  }
  if (!query.next()) {
    throw std::runtime_error("current observing site not found");
  }
  return site_from_query(query);
}

std::vector<ObservingSite> SiteRepository::fetch_all() const {
  QSqlQuery query = get_query();
  if (!query.exec("select id, name, latitude, longitude, height, temperature, humidity, "
                  "pressure, utc_offset from sites")) {
    throw std::runtime_error("get_observing_sites exec failed");
  }
  std::vector<ObservingSite> sites;
  while (query.next()) {
    sites.push_back(site_from_query(query));
  }
  return sites;
}

bool SiteRepository::create(ObservingSite &site) const {
  QSqlQuery query = get_query();
  query.prepare("insert into sites (name, latitude, longitude, height, temperature, humidity, "
                "pressure, utc_offset) values (?, ?, ?, ?, ?, ?, ?, ?)");
  query.addBindValue(QString::fromStdString(site.name));
  query.addBindValue(site.latitude);
  query.addBindValue(site.longitude);
  query.addBindValue(site.height);
  query.addBindValue(site.temperature);
  query.addBindValue(site.humidity);
  query.addBindValue(site.pressure);
  query.addBindValue(static_cast<int>(site.utc_offset.count()));
  if (query.exec()) {
    site.id = query.lastInsertId().toInt();
    return true;
  }
  return false;
}

bool SiteRepository::update(const ObservingSite &observing_site) const {
  QSqlQuery query = get_query();
  query.prepare(
      "update sites set name = ?, latitude = ?, longitude = ?, height = ?, temperature = ?, "
      "humidity = ?, pressure = ?, utc_offset = ? where id = ?");
  query.addBindValue(QString::fromStdString(observing_site.name));
  query.addBindValue(observing_site.latitude);
  query.addBindValue(observing_site.longitude);
  query.addBindValue(observing_site.height);
  query.addBindValue(observing_site.temperature);
  query.addBindValue(observing_site.humidity);
  query.addBindValue(observing_site.pressure);
  query.addBindValue(static_cast<int>(observing_site.utc_offset.count()));
  query.addBindValue(observing_site.id);
  return query.exec();
}

ObservingSite SiteRepository::site_from_query(const QSqlQuery &query) const {
  ObservingSite site;
  site.id = query.value(0).toInt();
  site.name = query.value(1).toString().toStdString();
  site.latitude = query.value(2).toDouble();
  site.longitude = query.value(3).toDouble();
  site.height = query.value(4).toInt();
  site.temperature = query.value(5).toInt();
  site.humidity = query.value(6).toInt();
  site.pressure = query.value(7).toInt();
  site.utc_offset = std::chrono::minutes(query.value(8).toInt());
  return site;
}

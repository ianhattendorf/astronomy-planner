#include "DownloadManager.hpp"
#include <algorithm>
#include <gsl/gsl_util>
#include <iostream>

DownloadManager::DownloadManager(QObject *parent) : QObject(parent) {
  connect(&manager_, &QNetworkAccessManager::finished, this, &DownloadManager::download_finished);
}

QNetworkReply *DownloadManager::do_download(const QUrl &url) {
  QNetworkRequest request{url};
  request.setAttribute(QNetworkRequest::FollowRedirectsAttribute, true);
  QNetworkReply *reply = manager_.get(request);

#if QT_CONFIG(ssl)
  connect(reply, &QNetworkReply::sslErrors, this, &DownloadManager::ssl_errors);
#endif

  current_downloads_.append({reply});
  return reply;
}

void DownloadManager::download_finished(QNetworkReply *reply) {
  const auto found_download =
      std::find_if(current_downloads_.begin(), current_downloads_.end(),
                   [reply](const auto &info) { return info.reply == reply; });
  Expects(found_download != current_downloads_.cend());
  current_downloads_.erase(found_download);

  emit finished(reply);
}

void DownloadManager::ssl_errors(const QList<QSslError> &errors) {
#if QT_CONFIG(ssl)
  for (const auto &error : errors) {
    qWarning().nospace() << "SSL error: " << qPrintable(error.errorString());
  }
#else
  Q_UNUSED(errors);
#endif
}

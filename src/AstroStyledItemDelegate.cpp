#include "AstroStyledItemDelegate.hpp"
#include "AstroHelper.hpp"

AstroStyledItemDelegate::AstroStyledItemDelegate(AstroStyleType style_type, QObject *parent)
    : QStyledItemDelegate(parent), style_type_(style_type) {}

QString AstroStyledItemDelegate::displayText(const QVariant &value,
                                             const QLocale & /*locale*/) const {
  if (value.isNull()) {
    return {};
  }

  bool parse_success;
  const double double_value = value.toDouble(&parse_success);
  if (!parse_success) {
    throw std::runtime_error("Failed to parse double");
  }

  if (style_type_ == AstroStyleType::HMS24 || style_type_ == AstroStyleType::HMS12PosNeg) {
    const HoursMinutesSeconds hms = AstroHelper::rad_to_hms(double_value);

    if (style_type_ == AstroStyleType::HMS24) {
      return QString{"%1h %2m %3s"}
          .arg(hms.hours, 2, 10, QChar{'0'})
          .arg(hms.minutes, 2, 10, QChar{'0'})
          .arg(hms.seconds, 2, 10, QChar{'0'});
    }

    if (style_type_ == AstroStyleType::HMS12PosNeg) {
      return QString{"%1%2:%3:%4"}
          .arg(hms.sign)
          .arg(hms.hours, 2, 10, QChar{'0'})
          .arg(hms.minutes, 2, 10, QChar{'0'})
          .arg(hms.seconds, 2, 10, QChar{'0'});
    }
  }

  const DegreesMinutesSeconds dms = AstroHelper::rad_to_dms(double_value);

  if (style_type_ == AstroStyleType::DMS360) {
    return QString{"%1°%2'%3\""}
        .arg(dms.degrees, 3, 10, QChar{'0'})
        .arg(dms.arcmin, 2, 10, QChar{'0'})
        .arg(dms.arcsec, 2, 10, QChar{'0'});
  }

  if (style_type_ == AstroStyleType::DMS90PosNeg) {
    return QString{"%1%2°%3'%4\""}
        .arg(dms.sign)
        .arg(dms.degrees, 2, 10, QChar{'0'})
        .arg(dms.arcmin, 2, 10, QChar{'0'})
        .arg(dms.arcsec, 2, 10, QChar{'0'});
  }

  if (style_type_ == AstroStyleType::D360) {
    return QString{"%1°"}.arg(dms.degrees);
  }

  if (style_type_ == AstroStyleType::D90PosNeg) {
    if (dms.sign == '-') {
      return QString{"%1%2°"}.arg(dms.sign).arg(dms.degrees);
    }
    return QString{"%1°"}.arg(dms.degrees);
  }

  throw std::runtime_error("Unknown AstroStyleType");
}

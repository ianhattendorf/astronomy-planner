#include "MainWindow.hpp"
#include <QApplication>
#include <QDateTime>
#include <QTextStream>
#include <cstdlib>

void message_handler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  QApplication::setApplicationName("AstronomyPlanner");
  QApplication::setOrganizationDomain("ianhattendorf.com");
  QApplication::setOrganizationName("Ian Hattendorf");

  qInstallMessageHandler(message_handler);

  MainWindow w;
  w.show();

  return QApplication::exec();
}

void message_handler(QtMsgType type, const QMessageLogContext & /*context*/, const QString &msg) {
  QTextStream out(stdout);
  out << '[' << QDateTime::currentDateTime().toString(Qt::ISODateWithMs) << "] ";
  switch (type) {
  case QtDebugMsg:
    out << "[DEBUG] ";
    break;
  case QtInfoMsg:
    out << "[INFO] ";
    break;
  case QtWarningMsg:
    out << "[WARN] ";
    break;
  case QtCriticalMsg:
    out << "[CRITICAL] ";
    break;
  case QtFatalMsg:
    out << "[FATAL] ";
    break;
  default:
    out << "[UNKNOWN] ";
    break;
  }
  out << msg << '\n';
}

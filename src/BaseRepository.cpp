#include "BaseRepository.hpp"

BaseRepository::BaseRepository(Database database) : database_(std::move(database)) {}

void BaseRepository::begin_transaction() const {
  database_.begin_transaction();
}

void BaseRepository::end_transaction() const {
  database_.end_transaction();
}

QSqlQuery BaseRepository::get_query() const {
  return database_.get_query();
}

#pragma once

#include "AstroStructs.hpp"
#include <chrono>
#include <erfam.h>
#include <string>
#include <tuple>
#include <utility>
#include <variant>

namespace AstroHelper {
/*!
 * \brief Compute star-independent parameters
 * \param observing_parameters \ref ObservingParameters
 * \param offset Time offset
 * \return Star-independent parameters
 */
eraASTROM get_astrom(const ObservingParameters &observing_parameters, std::chrono::seconds offset);

/*!
 * \brief Get Greenwich Apparent Sidereal Time
 * \param offset Time offset
 * \return GAST in radians
 */
double get_gast(std::chrono::seconds offset);

/*!
 * \brief Calculate the cosine hour angle at the specified \p latitude, \p declination, and \p
 * altitude
 * \param declination Object declination
 * \param latitude Observing latitude
 * \param altitude Horizon altitude
 * \return The corresponding cosine hour angle. If <= -1.0, object never
 * drops below \p altitude. If >= 1.0, object never reaches \p altitude.
 */
double cos_ha_at_alt(double declination, double latitude, double altitude);

/*!
 * \brief Get rise, transit, and set times
 * \param right_ascension Object right ascension
 * \param declination Object declination
 * \param latitude Observing latitude
 * \param altitude Horizon altitude
 * \return std::tuple of local sidereal times for rise, transit, and set times
 */
std::tuple<std::variant<double, RiseSet>, double, std::variant<double, RiseSet>>
rise_transit_set_lst(double right_ascension, double declination, double latitude, double altitude);

/*!
 * \brief Calculate the altitude at transit
 * \param declination Object declination
 * \param latitude Observing latitude
 * \return Altitude at transit
 */
double transit_alt(double declination, double latitude);

/*!
 * \brief International Celestial Reference System to observed.
 *
 * NOTE: Internal functions require non-const eraASTROM, maybe we should just copy?
 *
 * \param ra Right Ascension in radians
 * \param dec Declination in radians
 * \param astrom Pre-computed star-independent parameters
 * \return \ref AstroObserved
 */
AstroObserved icrs_to_observed(double ra, double dec, eraASTROM astrom);

/*!
 * \brief Convert radians to degrees, arcmin, arcsec
 * \param rad Radians to convert
 * \return \ref DegreesMinutesSeconds
 */
DegreesMinutesSeconds rad_to_dms(double rad);

/*!
 * \brief Convert radians to hours, minutes, seconds
 * \param rad Radians to convert
 * \return \ref HoursMinutesSeconds
 */
HoursMinutesSeconds rad_to_hms(double rad);

/*!
 * \brief Convert decimal degrees to radians
 * \param dd Decimal degrees to convert
 * \return Radians
 */
constexpr double dd_to_rad(double dd) {
  return dd * ERFA_DD2R;
}

/*!
 * \brief Convert decimal degrees to radians
 * \param dd Decimal degrees to convert
 * \return Radians
 */
constexpr AstroObserved dd_to_rad(const AstroObserved &dd) {
  AstroObserved rad{};
  rad.azimuth = dd_to_rad(dd.azimuth);
  rad.zenith = dd_to_rad(dd.zenith);
  rad.hour_angle = dd_to_rad(dd.hour_angle);
  rad.ra = dd_to_rad(dd.ra);
  rad.dec = dd_to_rad(dd.dec);
  return rad;
}

/*!
 * \brief Convert radians to decimal degrees
 * \param Radians to convert
 * \return Decimal degrees
 */
constexpr double rad_to_dd(double rad) {
  return rad * ERFA_DR2D;
}

/*!
 * \brief Convert radians to decimal degrees
 * \param Radians to convert
 * \return Decimal degrees
 */
constexpr AstroObserved rad_to_dd(const AstroObserved &rad) {
  AstroObserved dd{};
  dd.azimuth = rad_to_dd(rad.azimuth);
  dd.zenith = rad_to_dd(rad.zenith);
  dd.hour_angle = rad_to_dd(rad.hour_angle);
  dd.ra = rad_to_dd(rad.ra);
  dd.dec = rad_to_dd(rad.dec);
  return dd;
}

/*!
 * \brief Wrap a radian to the range [-pi, pi)
 * \param rad The radian to wrap
 * \return Radian in the stated range
 */
double rad_wrap_negative_pi(double rad);

/*!
 * \brief Wrap a radian to the range [0, 2pi)
 * \param rad The radian to wrap
 * \return Radian in the stated range
 */
double rad_wrap_positive_two_pi(double rad);

/*!
 * \brief Convert hours, minutes, seconds to radians
 * \param sign Sign, '-' or '+'
 * \param hours Hours to convert
 * \param minutes Minutes to convert
 * \param seconds Seconds to convert
 * \return \ref double
 */
double hms_to_rad(char sign, int hours, int minutes, int seconds);

constexpr int frac_resolution = 9;
} // namespace AstroHelper

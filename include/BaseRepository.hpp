#pragma once

#include "Database.hpp"

class BaseRepository {
public:
  BaseRepository(Database database);
  virtual ~BaseRepository() = default;

  void begin_transaction() const;
  void end_transaction() const;
  QSqlQuery get_query() const;

protected:
  Database database_;
};

#pragma once

#include <QDateTime>
#include <QObject>
#include <chrono>

class TimeKeeper : public QObject {
  Q_OBJECT
public:
  explicit TimeKeeper(QObject *parent = nullptr);
  QDateTime local_time() const;
  QDateTime local_offset_time() const;
  std::chrono::seconds offset() const;

signals:
  void offset_changed();
  void utc_offset_changed();

public slots:
  void set_offset(std::chrono::seconds offset);
  void set_utc_offset(std::chrono::seconds utc_offset);

private:
  std::chrono::seconds offset_{};
  std::chrono::seconds utc_offset_{};
};

#pragma once

#include <QMetaType>
#include <QNetworkReply>
#include <QString>
#include <QUrl>
#include <chrono>
#include <erfam.h>
#include <optional>
#include <string>

enum class RiseSet { Always, Never };

enum class SkySurvey { DSS, SDSS };

Q_DECLARE_METATYPE(SkySurvey)

struct CurrentSettings {
  int id;
  int current_site_id;
};

struct ObservingSite {
  int id = -1;
  std::string name;
  double latitude;
  double longitude;
  int height;
  int temperature;
  int humidity;
  int pressure;
  std::chrono::minutes utc_offset;
};

struct ObservingParameters {
  double latitude;
  double longitude;
  double height;
  double temperature;
  double humidity;
  double pressure;
};

struct AstroObserved {
  double azimuth;
  double zenith;
  double hour_angle;
  double ra;
  double dec;

  constexpr double altitude(bool rad) const { return (rad ? ERFA_DPI / 2 : 90.0) - zenith; }
};

struct HoursMinutesSeconds {
  char sign;
  int hours;
  int minutes;
  int seconds;
  int fraction;

  /*!
   * \brief Convert \ref HoursMinutesSeconds to string
   *
   * \param Number of decimal places for seconds to include
   * \return \ref std::string
   */
  std::string to_string(int decimal_places = -1) const;

  constexpr std::chrono::seconds seconds_since_start() {
    return (sign == '-' ? -1 : 1) * (std::chrono::hours(hours) + std::chrono::minutes(minutes) +
                                     std::chrono::seconds(seconds)); // TODO subsecond precision
  }
};

struct DegreesMinutesSeconds {
  char sign;
  int degrees;
  int arcmin;
  int arcsec;
  int fraction;
};

struct AstroObject {
  int id = -1;
  QString object_id;
  std::optional<SkySurvey> preferred_survey_image;
  QString common_names;
  double ra;
  double dec;
  double maj_ax;
  double min_ax;
  double b_mag;
  double v_mag;

  bool operator==(const AstroObject &other) const;
};

enum class HeaderType {
  Id,
  ObjectId,
  Names,
  Ra,
  Dec,
  MajorAxis,
  MinorAxis,
  BMag,
  VMag,
  Az,
  Alt,
  Visible,
  Rise,
  Set,
  Transit,
  TransitAlt,
  HourAngle,
};

struct HeaderInfo {
  HeaderType type;
  QString header;
  QString tooltip;
};

enum class SqlStatus { None, Insert, Delete };

template <typename T> struct SqlCache {
  T object;
  SqlStatus status = SqlStatus::None;
  bool dirty = false;
};

struct ImageQueueInfo {
  QUrl url_;
  QString path_;
  QString name_;
  SkySurvey survey_;

  ImageQueueInfo(QUrl url, QString path, QString name, SkySurvey survey);

  QUrl url() const { return url_; }
  QString path() const { return path_; }
  QString name() const { return name_; }
  SkySurvey survey() const { return survey_; }
};

struct ImageDownloadInfo {
  QNetworkReply *reply_;
  QString path_;
  QString name_;
  SkySurvey survey_;

  ImageDownloadInfo(QNetworkReply *reply, const ImageQueueInfo &info);

  QNetworkReply *reply() const { return reply_; }
  QString path() const { return path_; }
  QString name() const { return name_; }
  SkySurvey survey() const { return survey_; }
};

#pragma once

#include "BaseRepository.hpp"
#include "Database.hpp"

class SiteRepository : public BaseRepository {
public:
  SiteRepository(Database database);

  ObservingSite fetch(int id) const;

  std::vector<ObservingSite> fetch_all() const;

  bool create(ObservingSite &site) const;

  bool update(const ObservingSite &observing_site) const;

private:
  ObservingSite site_from_query(const QSqlQuery &query) const;
};

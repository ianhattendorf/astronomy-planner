#pragma once

#include "AstroStructs.hpp"
#include "SettingsRepository.hpp"
#include "SiteRepository.hpp"
#include <QDialog>
#include <QString>
#include <chrono>

namespace Ui {
class ObservingSites;
}
class TimeKeeper;

class ObservingSites : public QDialog {
  Q_OBJECT

public:
  explicit ObservingSites(SiteRepository site_repository, SettingsRepository settings_repository,
                          QWidget *parent = nullptr);
  ~ObservingSites();

  void accept() override;

  ObservingSite observing_site() const;

private slots:
  void on_siteComboBox_currentIndexChanged(int index);

private:
  double parse_dd(const QString &dd) const;
  std::chrono::minutes hours_to_minutes(double hours) const;
  void update_site(ObservingSite site);

  Ui::ObservingSites *ui;
  SiteRepository site_repository_;
  SettingsRepository settings_repository_;
  ObservingSite observing_site_{};
};

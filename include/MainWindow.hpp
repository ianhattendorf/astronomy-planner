#pragma once

#include "AstroStructs.hpp"
#include "SkySurveyHelper.hpp"
#include <QMainWindow>
#include <QQueue>
#include <QUrl>
#include <chrono>
#include <memory>
#include <vector>

namespace Ui {
class MainWindow;
}
class AstroObjectTableModel;
class ClickableLabel;
class Database;
class SettingsRepository;
class SiteRepository;
class DownloadManager;
class QNetworkReply;
class QPixmap;
class TimeKeeper;

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

  void closeEvent(QCloseEvent *event) override;

private slots:
  void on_sqlRevertButton_clicked();
  void on_sqlSubmitButton_clicked();
  void on_sqlNewRowButton_clicked();
  void on_sqlDeleteRowsButton_clicked();
  void on_objectImageComboBox_currentIndexChanged(int index);
  void on_objectImageComboBox_activated(int index);
  void on_dateTimeEdit_editingFinished();
  void on_pushButton_clicked();
  void download_catalog_start();
  void download_catalog_finished(QNetworkReply *reply);
  void download_images_start();
  void download_images_finished(QNetworkReply *reply);
  void image_clicked();
  void table_row_changed(const QModelIndex &current, const QModelIndex &previous);
  void edit_sites();

private:
  QString get_db_path() const;
  QString get_dss_path() const;
  QString get_sdss_path() const;
  void parse_and_save_catalog(const QByteArray &catalog_data) const;
  QString get_settings_path() const;
  QString get_image_path(SkySurvey survey, const QString &name) const;
  void download_next_queued_image();
  void set_observing_site(ObservingSite observing_site);

  Ui::MainWindow *ui;
  AstroObjectTableModel *model_;
  DownloadManager *catalog_download_manager_;
  DownloadManager *images_download_manager_;
  std::unique_ptr<Database> database_;
  std::unique_ptr<SettingsRepository> settings_repository_;
  std::unique_ptr<SiteRepository> site_repository_;
  std::unique_ptr<QPixmap> object_preview_image_ = nullptr;
  ClickableLabel *obect_image_label_;

  QNetworkReply *catalog_reply_ = nullptr;
  std::vector<ImageDownloadInfo> image_replies_;
  QQueue<ImageQueueInfo> image_queue_;
  TimeKeeper *time_keeper_;

  CurrentSettings current_settings_{};
  ObservingSite observing_site_{};

  const std::vector<SkySurvey> surveys_{SkySurvey::SDSS, SkySurvey::DSS};
  const std::string catalog_header_ =
      "Name;Type;RA;Dec;Const;MajAx;MinAx;PosAng;B-Mag;V-Mag;J-Mag;H-Mag;K-"
      "Mag;SurfBr;Hubble;Cstar U-Mag;Cstar B-Mag;Cstar V-Mag;M;NGC;IC;Cstar "
      "Names;Identifiers;Common names;NED notes;OpenNGC notes";
  // arcminutes
  static constexpr double default_image_size = 15.0;
  static constexpr int max_concurrent_downloads = 5;
  static constexpr int image_num_pixels = 1024;
};

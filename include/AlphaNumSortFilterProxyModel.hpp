#pragma once

#include <QSortFilterProxyModel>

class AlphaNumSortFilterProxyModel : public QSortFilterProxyModel {
public:
  explicit AlphaNumSortFilterProxyModel(QObject *parent = nullptr);
  bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;
};

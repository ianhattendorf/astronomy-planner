#pragma once

#include "BaseRepository.hpp"
#include "Database.hpp"

class SettingsRepository : public BaseRepository {
public:
  SettingsRepository(Database database);

  CurrentSettings fetch(int id) const;
  bool update(const CurrentSettings &settings) const;
};

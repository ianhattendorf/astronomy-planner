#pragma once

#include "AstroStructs.hpp"
#include "BaseRepository.hpp"
#include "Database.hpp"
#include "SkySurveyHelper.hpp"
#include <initializer_list>
#include <string>
#include <string_view>
#include <vector>

class AstroObjectRepository : public BaseRepository {
public:
  AstroObjectRepository(Database database);

  std::vector<AstroObject> fetch_all() const;
  bool create(AstroObject &astro_object) const;
  bool update(const AstroObject &astro_object) const;
  bool del(const AstroObject &astro_object) const;

  void fill_astro_object_data(AstroObject &astro_object) const;

private:
  void build_coalesce_single(std::ostream &os, int count, std::string_view field_name) const;
  std::string build_coalesce_multiple(int count,
                                      std::initializer_list<std::string_view> fields) const;

  const QString astro_object_coalesce_ = QString::fromStdString(build_coalesce_multiple(
      4, {"common_names", "ra", "dec", "maj_ax", "min_ax", "b_mag", "v_mag"}));
  const QString astro_object_join_ =
      "left outer "
      "join catalog as c1 on c1.name = objects.object_id left outer "
      "join catalog as c2 on 'M' || c2.m_id = objects.object_id "
      "left outer join catalog as c3 on 'NGC' || c3.ngc_id = objects.object_id "
      "left outer join catalog as c4 on 'IC' || c4.ic_id = objects.object_id";
};

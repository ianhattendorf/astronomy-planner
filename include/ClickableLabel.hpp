#pragma once

#include <QLabel>
#include <QObject>
#include <QWidget>
#include <Qt>

class ClickableLabel : public QLabel {
  Q_OBJECT
public:
  explicit ClickableLabel(QWidget *parent = nullptr, Qt::WindowFlags flags = Qt::WindowFlags{});

  void setPixmapScaled(const QPixmap &pixmap);

signals:
  void clicked();
  void resized(const QSize &size);

protected:
  void mousePressEvent(QMouseEvent *event) override;
  void resizeEvent(QResizeEvent *event) override;
};

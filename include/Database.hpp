#pragma once

#include "AstroStructs.hpp"
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <vector>

class Database {
public:
  Database(const QString &db_driver, QString db_name, const QString &db_path);
  void db_upgrade() const;
  QSqlQuery get_query() const;
  void begin_transaction() const;
  void end_transaction() const;
  QSqlDatabase get_database() const;

private:
  int get_database_version() const;

  /*!
   * \brief Execute a SQL script.
   *
   * Qt SQLite driver doesn't support mutliple statements. The script will be naively transformed
   * into multiple statements by splitting on semicolons. If there are any semicolons that aren't
   * statement terminators (in comments, string literals, etc.) then the script will fail.
   *
   * \param database The database to execute the script on
   * \param path Path the the script file
   */
  void execute_sql_script(const QString &path) const;

  static constexpr int database_version = 5;
  QString db_name_;
};

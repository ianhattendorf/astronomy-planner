#pragma once

#include "AstroObjectRepository.hpp"
#include "AstroStructs.hpp"
#include "SkySurveyHelper.hpp"
#include <QAbstractTableModel>
#include <QList>
#include <QVector>
#include <chrono>
#include <erfam.h>
#include <initializer_list>
#include <iosfwd>
#include <optional>
#include <string>
#include <string_view>

class TimeKeeper;
class Database;

class AstroObjectTableModel : public QAbstractTableModel {
  Q_OBJECT
public:
  AstroObjectTableModel(QObject *parent, AstroObjectRepository object_repository,
                        TimeKeeper *time_keeper);
  int rowCount(const QModelIndex &parent) const override;
  int columnCount(const QModelIndex &parent) const override;
  QVariant data(const QModelIndex &index, int role) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
  bool setData(const QModelIndex &index, const QVariant &value, int role) override;
  bool insertRows(int row, int count, const QModelIndex &index) override;
  bool removeRows(int row, int count, const QModelIndex &index) override;
  Qt::ItemFlags flags(const QModelIndex &index) const override;
  void set_observing_parameters(ObservingParameters observing_parameters);
  bool is_dirty() const;

public slots:
  void revertAll();
  bool submitAll();
  void tick();

private:
  void refresh_objects();
  void fill_astro_object_data(AstroObject &astro_object) const;
  void update_observables();
  void set_headers();
  int header_column(HeaderType type) const;

  QList<SqlCache<AstroObject>> astro_objects_;
  AstroObjectRepository object_repository_;
  eraASTROM astrom_{};
  double greenwich_sidereal_time_{};
  double local_sidereal_time_{};
  ObservingParameters observing_parameters_{};

  TimeKeeper *time_keeper_ = nullptr;

  QVector<HeaderInfo> headers_;
};

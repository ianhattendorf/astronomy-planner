#pragma once

#include <QStyledItemDelegate>

enum class AstroStyleType { HMS24, HMS12PosNeg, DMS360, DMS90PosNeg, D360, D90PosNeg };

class AstroStyledItemDelegate : public QStyledItemDelegate {
public:
  AstroStyledItemDelegate(AstroStyleType style_type, QObject *parent = nullptr);
  QString displayText(const QVariant &value, const QLocale &locale) const override;

private:
  const AstroStyleType style_type_;
};

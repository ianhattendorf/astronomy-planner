#pragma once

#include <QObject>
#include <QtCore>
#include <QtNetwork>

struct DownloadInfo {
  QNetworkReply *reply;
};

class DownloadManager : public QObject {
  Q_OBJECT

public:
  explicit DownloadManager(QObject *parent = nullptr);
  QNetworkReply *do_download(const QUrl &url);

signals:
  void finished(QNetworkReply *reply);

public slots:

private slots:
  void download_finished(QNetworkReply *reply);
  void ssl_errors(const QList<QSslError> &errors);

private:
  QNetworkAccessManager manager_;
  QVector<DownloadInfo> current_downloads_;
};

#pragma once

#include "AstroStructs.hpp"
#include <string>
#include <string_view>

namespace SkySurveyHelper {
/*!
 * \brief Get URL for Sky Survey image
 * \param ra Right Ascension in decimal degrees
 * \param dec Declination in decimal degrees
 * \param size FOV in arc minutes
 * \return
 */
std::string get_url(SkySurvey survey, double ra, double dec, double size, int pixels);

/*!
 * \brief Get human readable name for SkySurvey
 * \param survey SkySurvey to get name of
 * \return Name of the SkySurvey
 */
std::string get_survey_name_short(SkySurvey survey);

/*!
 * \brief Get a SkySurvey from its human readable name
 * \param name Name of SkySurvey to get
 * \return SkySurvey
 */
SkySurvey get_survey_from_name_short(std::string_view name);
} // namespace SkySurveyHelper

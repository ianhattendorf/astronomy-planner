#pragma once

#include <QTableView>

class ObjectTableView : public QTableView {
public:
  ObjectTableView(QWidget *parent = nullptr);

protected:
  void keyPressEvent(QKeyEvent *event) override;
};

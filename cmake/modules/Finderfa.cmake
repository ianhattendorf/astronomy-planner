# Find erfa
#
# Defined targets:
#  erfa::erfa

find_package(PkgConfig REQUIRED)
pkg_check_modules(PC_ERFA erfa)

find_library(erfa_LIBRARY
        DOC "Path to erfa library."
        NAMES erfa
        HINTS ${PC_ERFA_LIBDIR}
            ${PC_ERFA_LIBRARY_DIRS})

find_path(erfa_INCLUDE_DIRS
        DOC "Path to erfa include directory."
        NAMES erfa.h
        HINTS ${PC_ERFA_INCLUDEDIR}
            ${PC_ERFA_INCLUDE_DIRS})

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(erfa REQUIRED_VARS erfa_INCLUDE_DIRS erfa_LIBRARY)

add_library(erfa::erfa UNKNOWN IMPORTED)
set_target_properties(erfa::erfa PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${erfa_INCLUDE_DIRS}"
        IMPORTED_LOCATION ${erfa_LIBRARY})

# Find libcsv
#
# Defined targets:
#  libcsv::libcsv

find_library(libcsv_LIBRARY
        DOC "Path to libcsv library."
        NAMES csv)

find_path(libcsv_INCLUDE_DIRS
        DOC "Path to libcsv include directory."
        NAMES csv.h)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(libcsv REQUIRED_VARS libcsv_INCLUDE_DIRS libcsv_LIBRARY)

add_library(libcsv::libcsv UNKNOWN IMPORTED)
set_target_properties(libcsv::libcsv PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${libcsv_INCLUDE_DIRS}"
        IMPORTED_LOCATION ${libcsv_LIBRARY})

update app_info set db_version = 3 where id = 1;

drop table catalog;

create table catalog (id integer primary key autoincrement, name text, type text, ra number, dec number, const text, maj_ax number, min_ax number, pos_ang text, b_mag number, v_mag number, j_mag number, h_mag number, k_mag number, surf_br number, hubble text, cstar_u_mag text, cstar_b_mag text, cstar_v_mag text, m_id integer, ngc_id integer, ic_id integer, cstar_names text, identifiers text, common_names text, ned_notes text, open_ngc_notes text);

update app_info set db_version = 5 where id = 1;

create table sites (id integer primary key autoincrement, name text, latitude double precision, longitude double precision, height smallint, temperature smallint, humidity smallint, pressure smallint, utc_offset smallint);

insert into sites (name, latitude, longitude, height, temperature, humidity, pressure, utc_offset)
values
("Lowell Observatory", 0.61431822027, -1.94907298494, 2210, 25, 10, 1005, -7 * 60);

create table current_settings (id integer primary key autoincrement, current_site references sites(id));

insert into current_settings (current_site)
values
(1);

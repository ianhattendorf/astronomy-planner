update app_info set db_version = 4 where id = 1;

create table sky_survey (id integer primary key autoincrement, name text);

alter table objects rename to old_objects;

create table objects (id integer primary key autoincrement, object_id text, date_created text, preferred_survey_image references sky_survey(id));

insert into objects (id, object_id, date_created)
  select id, object_id, date_created
  from old_objects;

drop table old_objects;

insert into sky_survey (name) values ('SDSS'), ('DSS');

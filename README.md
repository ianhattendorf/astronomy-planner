# astronomy-planner

Qt program to plan astronomical observations. Currently only tested on Fedora 28. Still very much a work in progress.

![AstronomyPlanner Main Page](AstronomyPlanner.png)

## Quick Start

Download the DSO catalog and set your observing site via the file menu. Object information will be automatically looked up and displayed if it exists in the catalog. Images can be downloaded for all current objects as well.

## Building

Requires a compiler that supports C++17. Tested on GCC 7.3 and Clang 5/6.

### Install Dependencies

- Qt
- Microsoft Guidelines Support Library

### Generate Build Files

```sh
$ cd <project_root>
$ mkdir build && cd build && cmake -G Ninja -DCMAKE_BUILD_TYPE=Debug ..
```

### Compile

```sh
$ cd <project_root>/build
$ ninja
```
